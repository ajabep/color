#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#if !defined(BUFFER_SIZE) || BUFFER_SIZE > SSIZE_MAX
/* Max : 0x7ffff000 (check `read` for more details) */
#define BUFFER_SIZE 2048
#endif

#ifndef COLOR
#define COLOR       "\033[31m"
#endif
#ifndef RESET_COLOR
#define RESET_COLOR "\033[39m"
#endif

#define EXIT_READ_FAILURE 1

int main(int ac, char** av) {
	int fd_stdin;
	char* word_to_colorize;
	size_t len_word;
	ssize_t read_chars;
	char buffer[BUFFER_SIZE];
	char* buffer_ptr;
	size_t len_chars_to_write;
	int offset;

	//char* start_ptr;
	//size_t len_chars_to_write;

	if (ac != 2 || !strcmp(av[1], "-h") || !strcmp(av[1], "-help") || !strcmp(av[1], "--help")) {
		printf("Usage: %s WORD\n", av[0]);
		printf("\n"
		       "\tWill colorize the WORD.\n"
		       "\tThis programm has been made for real-time display: as soon \n"
		       "\ta char is available, as soon it will be treat.\n");
		printf("\n");
		printf("Usage: %s -h\n", av[0]);
		printf("Usage: %s -help\n", av[0]);
		printf("Usage: %s --help\n", av[0]);
		printf("\n"
			   "\tDisplay this help message\n");
		return 0;
	}

	/* Fill variables */
	fd_stdin = fileno(stdin);
	word_to_colorize = av[1];
 	len_word = strlen(word_to_colorize);

	/* unbuffer-ize stdin andstdout */
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stdin, NULL, _IONBF, 0);

	/* Youp */
	while (1) {
		read_chars = read(fd_stdin, buffer, BUFFER_SIZE);

		if (read_chars < 0) {

			if (errno == EAGAIN || errno == EWOULDBLOCK)
				continue;

			perror(NULL);
			return EXIT_READ_FAILURE;
		}

		if (read_chars == 0) {
			return EXIT_SUCCESS;
		}

		/* Use temporary variables, to modify them without loose original
		 * values*/
		buffer_ptr = buffer;
		len_chars_to_write = (size_t)read_chars;

		offset = 0;
		while(len_chars_to_write - offset >= len_word) {
			if (!memcmp(buffer_ptr + offset, word_to_colorize, len_word)) {
				fwrite(buffer_ptr, sizeof(char), offset, stdout);
				buffer_ptr += offset;
				len_chars_to_write -= offset;

				fwrite(COLOR, sizeof(char), strlen(COLOR), stdout);
				fwrite(buffer_ptr, sizeof(char), len_word, stdout);
				fwrite(RESET_COLOR, sizeof(char), strlen(RESET_COLOR), stdout);

				buffer_ptr += len_word;
				len_chars_to_write -= len_word;
				offset = 0;
			}
			else {
				++offset;
			}
		}
		fwrite(buffer_ptr, sizeof(char), len_chars_to_write, stdout);
	}
}

