Color
=====

Quick C program to colorize word found in stdin.

It works on real time and "on one line" flow, using unbuffered stdin and stdout,
as the display text of `mplayer`.
