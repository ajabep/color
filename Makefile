
PROGRAM_NAME=color

ifeq "$(CFLAGS)" ""
CFLAGS+=-Wall -Wextra -Wpedantic -Werror
ifeq "$(CLANG)" "clang"
CFLAGS+=-fsanitize=safe-stack,cfi
endif
CFLAGS+=-fpic -fPIE
CFLAGS+=-fvisibility=hidden -fstack-check -fstack-protector-all -Wstack-protector -Wformat -Wformat-security -fno-exceptions
CFLAGS+=--param ssp-buffer-size=4 -D_FORTIFY_SOURCE=2 -O2
endif

ifeq "$(LDFLAGS)" ""
ifeq "$(CLANG)" "clang"
LDFLAGS+=-flto=thun
endif
LDFLAGS+=-pie
LDFLAGS+=-Wl,-z,relro,-z,now -Wl,--no-undefined -Wl,--build-id=sha1 -rdynamic
endif

ifneq "$(COLOR)" ""
CFLAGS+=-DCOLOR='"$(COLOR)"'
endif

ifneq "$(RESET_COLOR)" ""
CFLAGS+=-DRESET_COLOR='"$(RESET_COLOR)"'
endif


all: compile

compile: color.o
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(PROGRAM_NAME) $^

%.o: %.c
	$(CC) $(CFLAGS) -c $^

clean:
	rm *.o
	rm $(PROGRAM_NAME)
